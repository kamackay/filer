module gitlab.com/kamackay/filer

go 1.15

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/gzip v0.0.1
	github.com/gin-gonic/gin v1.6.2
	github.com/google/uuid v1.1.1
	github.com/kamackay/goffmpeg v0.5.1
	github.com/robfig/cron/v3 v3.0.1
	github.com/sirupsen/logrus v1.4.2
	gopkg.in/yaml.v2 v2.3.0
)
